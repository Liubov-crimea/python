from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome(
    executable_path='E:\\Python\\Python_my code\\py_selenium\\chromedriver.exe')

base_url = 'https://www.saucedemo.com/'
driver.get(base_url)
driver.maximize_window()

login_standard_user = "standard_user"
password_all = "secret_sauce"


user_name = driver.find_element(By.XPATH, "//*[@id='user-name']")
user_name.send_keys(login_standard_user)
print("Input Login")
user_password = driver.find_element(By.XPATH, "//*[@id='password']")
user_password.send_keys(password_all)
print("Input Password")
button_login = driver.find_element(By.XPATH, "//*[@id='login-button']").click()
print("Click Login Button")

print("_____________________________")

"""INFO Product #1"""  # аннотация
product_1 = driver.find_element(By.XPATH, "//a[@id='item_4_title_link']")
value_product_1 = product_1.text
print(value_product_1)

price_product_1 = driver.find_element(
    By.XPATH, "//div[@class='inventory_item_price']")
value_price_product_1 = price_product_1.text
print(value_price_product_1)

select_product_1 = driver.find_element(
    By.XPATH, "//*[@id='add-to-cart-sauce-labs-backpack']")
select_product_1.click()
print("select_product")

# переходим в корзину
cart = driver.find_element(
    By.XPATH, "//a[@class='shopping_cart_link']").click()
print("Enter cart")

print("_____________________________")

"""INFO Card Product #1"""  # аннотация продукт

cart_product_1 = driver.find_element(By.XPATH, "//a[@id='item_4_title_link']")
cart_value_product_1 = cart_product_1.text
print(cart_value_product_1)
assert value_product_1 == cart_value_product_1
print("INFO Cart Product GOOD")

cart_price_product_1 = driver.find_element(
    By.XPATH, "//div[@class='inventory_item_price']")
cart_value_price_product_1 = cart_price_product_1.text
print(cart_value_price_product_1)

checkout = driver.find_element(
    By.XPATH, "//button[@class='btn btn_action btn_medium checkout_button']").click()
print("click checkout button")


print("_____________________________")

# ввести данные для покупки
"""Select User INFO"""
name_1 = "Alex"
surname = "Topat"


firstname = driver.find_element(By.XPATH, "//*[@id='first-name']")
firstname.send_keys(name_1)
print("input firstname")
lasttname = driver.find_element(By.XPATH, "//input[@id='last-name']")
lasttname.send_keys(surname)
print("input lasttname")
code = driver.find_element(By.XPATH, "//input[@id='postal-code']")
code.send_keys(1234)
print("input code")
time.sleep(2)
button_continue = driver.find_element(
    By.XPATH, "//input[@id='continue']").click()
print("press button continue")

print("_____________________________")

# проверка тот ли товар оформлен, сравн товара с гл стр и тог, что был добавлен в корзину
"""Info FINISH"""
finish_product_1 = driver.find_element(
    By.XPATH, "//div[@class='inventory_item_name']")
finish_value_product_1 = finish_product_1.text
print(finish_value_product_1)
assert finish_value_product_1 == finish_value_product_1
print("INFO finish Product GOOD")

finish_price_product_1 = driver.find_element(
    By.XPATH, "//div[@class='inventory_item_price']")
finish_value_price_product_1 = finish_price_product_1.text
print(finish_value_price_product_1)

# что цена в итоге совпад с ценой товара
summary_price = driver.find_element(
    By.XPATH, "//div[@class='summary_subtotal_label']")
value_summary_price = summary_price.text
print(value_summary_price)

item_total = "Item total: " + finish_value_price_product_1
print(item_total)
assert value_summary_price == item_total
print("Total summary price good")


driver.close()
